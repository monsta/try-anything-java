package mockito.spy;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Created by johnson on 2017/2/9.
 */
public class TestSpy {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Test
    public void testList() {
        List spyList = mock(List.class);

        when(spyList.get(0)).thenReturn("123");

        Object obj = spyList.get(0);
        System.out.println(obj);

        verify(spyList).get(0);
    }
}
