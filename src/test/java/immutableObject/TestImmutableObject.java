package immutableObject;

import org.joda.time.DateTime;
import org.junit.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * TestImmutableObject description
 *
 * @author johnson
 * @since 2015-07-21
 */
public class TestImmutableObject {

    @Test
    public void testImmutable() {
        ImmutableClass im = ImmutableClass.newInstance("originalString", 100, new DateTime()
            .withYear(2005).toDate());
        String firstToString = im.toString();
        System.out.println(firstToString);

        changeImmutableObject(im.getStrVal(), im.getIntVal(), im.getDateVal());
        String secondToString = im.toString();
        System.out.println(secondToString);

        assertThat(firstToString).isEqualTo(secondToString);
    }

    private void changeImmutableObject(String strVal, Integer intVal, Date dateVal) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        //strVal和intVal本來就是immutable，但Date的getter一定要回傳copy的object reference才有可能成功通過測試
        strVal = "newString";
        intVal = 9999;
        dateVal.setTime(new Date().getTime());
    }
}
