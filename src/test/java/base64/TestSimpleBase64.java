package base64;

import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by johnson on 2015/12/16.
 */
public class TestSimpleBase64 {
    @Test
    public void base54EncodeAndDecode() {
        final String expected = "Base64 is finally in Java!";

        String encodedString = Base64.getEncoder()
                .encodeToString(expected.getBytes(StandardCharsets.UTF_8));
        System.out.println(encodedString);
        final String actual = new String(Base64.getDecoder()
                .decode(encodedString),
                StandardCharsets.UTF_8);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void base64UrlEncodeAndDecode() {
        final String expected = "http://www.monsta.com/user?info=1234";

        String encodedString = Base64.getUrlEncoder()
                .encodeToString(expected.getBytes(StandardCharsets.UTF_8));
        System.out.println(encodedString);
        String actual = new String(Base64.getUrlDecoder()
                .decode(encodedString),
                StandardCharsets.UTF_8);


        assertThat(actual).isEqualTo(expected);
    }
}
