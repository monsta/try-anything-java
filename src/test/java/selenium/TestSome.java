package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * TestSome description
 *
 * @author johnson
 * @since 2015-07-17
 */
public class TestSome {
    public static void main(String[] args) throws Exception {
        WebDriver wd = new ChromeDriver();
        wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        wd.get("http://resgt.railway.gov.tw/groupticket/index.htm");
        wd.findElement(By.xpath("//div[5]/div[1]/span[2]")).click();
        wd.findElement(By.name("Image17")).click();
        wd.quit();
    }

    public static boolean isAlertPresent(FirefoxDriver wd) {
        try {
            wd.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }
}
