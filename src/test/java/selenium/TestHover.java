package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * TestHover description
 *
 * @author johnson
 * @since 2015-07-15
 */
public class TestHover {

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.get("http://the-internet.herokuapp.com/hovers");
        WebElement figure = driver.findElement(By.className("figure"));

        //模仿滑鼠移動的動作
        Actions actions = new Actions(driver).moveToElement(figure);
        actions.perform();

        assertThat(driver.findElement(By.className("figcaption")).isDisplayed()).isTrue();

        driver.quit();
    }
}
