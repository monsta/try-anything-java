package fibonacci;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

/**
 * 寫三種不同的fibonacci實作，簡單進行測試並印出跑了多少時間
 * 但，最簡單的實作無法傳入太大的參數，因為實在太慢了
 *
 * @author johnson
 * @since 2015-07-21
 */
public class FibonacciTest {

    private long start;
    @Before
    public void before() {
        start = new Date().getTime();
    }

    @After
    public void after() {
        System.out.println("start: " + start + " --- end: " + new Date().getTime());
    }

    @Test
    public void fibonacciSimple() {
        FibonacciSimple fibonacci = new FibonacciSimple();
        for (int i = 0; i < 40; i++) {
            System.out.println(fibonacci.fib(i));
        }
    }

    @Test
    public void fibonacciIteration() {
        FibonacciIteration fibonacci = new FibonacciIteration();
        for (int i = 0; i < 500; i++) {
            System.out.println(fibonacci.fib(i));
        }
    }

    @Test
    public void fibonacciCache() {
        FibonacciCache fibonacci = new FibonacciCache();
//        for(int i = 0; i < 500; i++) {
            System.out.println(fibonacci.fib(2000));
//        }
    }
}
