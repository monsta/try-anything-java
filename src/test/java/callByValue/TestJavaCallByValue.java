package callByValue;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * TestJavaCallByValue description
 *
 * @author johnson
 * @since 2015-12-18
 */
public class TestJavaCallByValue {
    @Test
    public void
    callAMethodWithObjectArgument_updateArgumentRefToNull_shouldNotChangeOriginalReference() {
        List<Integer> list = Arrays.asList(1, 2, 3);

        somMethodWouldNullifyArgument(list);

        Assert.assertNotNull(list);
    }

    private void somMethodWouldNullifyArgument(List<Integer> list) {
        list = null;
    }

    @Test
    public void
    callAMethodWithObjectArgument_updateArgumentContent_shouldUpdateOriginalObjectEither() {
        String originalName = "name";
        CustomPojo pojo = new CustomPojo(originalName, 20);

        someMethodWouldUpdateArgumentContent(pojo);

        assertThat(pojo.getName()).isNotEqualTo(originalName);
    }

    private void someMethodWouldUpdateArgumentContent(CustomPojo pojo) {
        pojo.setName("name2");
    }

    private class CustomPojo {
        private String name;
        private final int age;

        public CustomPojo(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
