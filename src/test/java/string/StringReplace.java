package string;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StringReplace {

    @Test
    public void replace() {
        String s = "abcdefgabcdefg";
        String replaced = s.replace("a", "z");
        assertThat(replaced).contains("z", "z");
    }
}
