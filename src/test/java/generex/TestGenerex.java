package generex;

import com.mifmif.common.regex.Generex;
import org.junit.Test;

public class TestGenerex {

    @Test
    public void generateRandomStringFromRegexPattern() {
        Generex gx = new Generex("[A-Z0-9]{8}");
        String randomString = gx.random();
        System.out.println(randomString);
    }
}
