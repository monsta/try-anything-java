package listRemoveDuplicate;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * RemoveDuplicateFromList description
 *
 * @author johnson
 * @since 2015-04-28
 */
public class RemoveDuplicateFromList {

    private List<Integer> listWihtDups = Arrays.asList(1, 2, 3, 2, 3, 1, 4);

    @Test
    public void usingPlainJava() {
        List<Integer> result = new ArrayList<>(new HashSet<>(listWihtDups));
        assertThat(result).hasSize(4);
    }

    @Test
    public void usingGuava() {
        ArrayList<Integer> result = Lists.newArrayList(Sets.newHashSet(listWihtDups));
        assertThat(result).hasSize(4);
    }

    @Test
    public void usingJava8StreamDistinct() {
        List<Integer> result = listWihtDups.stream().distinct().collect(toList());
        assertThat(result).hasSize(4);
    }
}
