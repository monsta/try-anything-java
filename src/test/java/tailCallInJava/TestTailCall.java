package tailCallInJava;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by johnsonchuang on 06/07/2017.
 */
public class TestTailCall {

    @Test
    public void noTailCall() {
        FactWithoutTailRecursion host = new FactWithoutTailRecursion();
        assertEquals(24, host.factorial(4));
    }

    @Test
    public void simpleTailCall() {
        FactWithTailRecursion host = new FactWithTailRecursion();
        assertEquals(24, host.factorial(4));
    }

    class FactWithTailRecursion {
        int factorial(int n) {
            if (n == 0) {
                return 1;
            } else {
                return fact(n - 1, 1);
            }
        }

        private int fact(int n, int acc) {
            if (n == 0) {
                return acc;
            } else {
                return fact(n - 1, acc * n);
            }
        }
    }

    private class FactWithoutTailRecursion {
        public int factorial(int n) {
            return (n <= 1 ? 1 : n * factorial(n - 1));
        }
    }
}
