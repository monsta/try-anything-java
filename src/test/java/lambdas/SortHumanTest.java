package lambdas;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.maxBy;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * SortHumanTest description
 *
 * @author johnson
 * @since 2015-04-16
 */
public class SortHumanTest {

    private List<Human> humans;

    @Before
    public void setUp() {
        humans = Lists.newArrayList(new Human("John", 20), new Human("Roe", 15));
    }

    @Test
    public void oldComparatorSort() {
        Collections.sort(humans, new Comparator<Human>() {
            @Override
            public int compare(Human h1, Human h2) {
                return h1.getName().compareTo(h2.getName());
            }
        });
        assertThat(humans.get(0).getName()).isEqualTo("John");
    }

    @Test
    public void basicSortWithLambdaSupport() {
        humans.sort((Human h1, Human h2) -> h1.getName().compareTo(h2.getName()));
        assertThat(humans.get(0).getName()).isEqualTo("John");
    }

    @Test
    public void basicSortWithLambdaSupportWithNoTypeDefinition() {
        humans.sort((h1, h2) -> h1.getName().compareTo(h2.getName()));
        assertThat(humans.get(0).getName()).isEqualTo("John");
    }

    @Test
    public void basicSortUsingMethodReference() {
        humans.sort(Human::compareByNameAndAge);
        assertThat(humans.get(0).getName()).isEqualTo("John");
    }

    @Test
    public void basicSortByComparatorComparing() {
        Collections.sort(humans, comparing(Human::getName));
    }

    @Test
    public void sortWith_java8ReversingComparator() {
        Comparator<Human> comparator = comparing(Human::getName);
        humans.sort(comparator.reversed());
        assertThat(humans.get(0).getName()).isEqualTo("Roe");
    }

    @Test
    public void sortByComplexConditions() {
        ArrayList<Human> list = getComplexHumanList();
        list.sort((lhu, rhu) -> {
            if(lhu.getName().equals(rhu.getName())) {
                return lhu.getAge() - rhu.getAge();
            } else {
                return lhu.getName().compareTo(rhu.getName());
            }
        });
        assertComplexSortResult(list);
    }

    private void assertComplexSortResult(ArrayList<Human> list) {
        assertThat(list.get(0).getName()).isEqualTo("John");
        assertThat(list.get(0).getAge()).isEqualTo(12);
    }

    private ArrayList<Human> getComplexHumanList() {
        return Lists.newArrayList(new Human("John", 15), new Human("John", 12)
            , new Human("Tony", 22));
    }

    @Test
    public void sortComplexCondition_withComposition() {
        ArrayList<Human> list = getComplexHumanList();
        list.sort(comparing(Human::getName).thenComparing(Human::getAge).reversed());
        assertComplexSortResultReversed(list);
    }

    private void assertComplexSortResultReversed(ArrayList<Human> list) {
        assertThat(list.get(0).getName()).isEqualTo("Tony");
        assertThat(list.get(0).getAge()).isEqualTo(22);
    }

    @Test
    public void streamCompareTest() {
        Function<Human, Integer> getAge = human -> human.getAge();
        Optional<Human> oldestHuman = getComplexHumanList()
            .stream()
            .collect(maxBy(comparing(getAge)));

        assertThat(oldestHuman.get().getName()).isEqualTo("Tony");
    }

    @Test
    public void joiningTest() {
        String allNamesJoined = getComplexHumanList().stream()
            .map(Human::getName)
            .collect(Collectors.joining(",", "{", "}"));
        assertThat(allNamesJoined).isEqualTo("{John,John,Tony}");
    }
}
