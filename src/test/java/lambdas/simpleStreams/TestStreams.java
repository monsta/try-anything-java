package lambdas.simpleStreams;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * Created by johnson on 2015/12/16.
 */
public class TestStreams {

    private Collection<Streams.Task> tasks;

    @Before
    public void setUp() {
        tasks = Arrays.asList(
                new Streams.Task(Streams.Status.OPEN, 5),
                new Streams.Task(Streams.Status.OPEN, 13),
                new Streams.Task(Streams.Status.CLOSED, 8)
        );
    }

    @Test
    public void sumOpenTaskPoints() {
        int expected = 5+13;

        int actual = tasks.stream()
                .filter(task -> task.getStatus() == Streams.Status.OPEN)
                .mapToInt(Streams.Task::getPoints)
                .sum();

        assertEquals(expected, actual);
    }

    @Test
    public void mapReduceToSummingPoints() {
        final Integer expected = 26;

        Integer actual = tasks.stream()
                .parallel()
                .map(Streams.Task::getPoints)
                .reduce(0, Integer::sum);

        assertEquals(expected, actual);
    }

    @Test
    public void simpleGrouping() {
        final Map<Streams.Status, List<Streams.Task>> taskGroup = tasks.stream()
                .collect(Collectors.groupingBy(Streams.Task::getStatus));

        assertThat(taskGroup).containsOnlyKeys(Streams.Status.OPEN, Streams.Status.CLOSED);
        assertThat(taskGroup).hasSize(taskGroup.entrySet().toArray().length);
    }
}
