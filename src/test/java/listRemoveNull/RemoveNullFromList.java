package listRemoveNull;

import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.PredicateUtils;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * RemoveNullFromList description
 *
 * @author johnson
 * @since 2015-04-27
 */
public class RemoveNullFromList {

    private List<?> listWithNull = Lists.newArrayList(null, 1, null);

    @Test
    public void removeUsingWhile() {
        //when remove success, it returns true
        while (listWithNull.remove(null)) ;

        assertThat(listWithNull).hasSize(1);
    }

    @Test
    public void removeAllNull() {
        listWithNull.removeAll(Collections.singleton(null));
        assertThat(listWithNull).hasSize(1);
    }

    @Test
    public void removeUsingGuavaWithPredicates() {
        Iterables.removeIf(listWithNull, Predicates.isNull());
        assertThat(listWithNull).hasSize(1);
    }

    @Test
    public void removeUsingApacheCommonsCollections_withFilter() {
        CollectionUtils.filter(listWithNull, PredicateUtils.notNullPredicate());
        assertThat(listWithNull).hasSize(1);
    }

    @Test
    public void removeUsingJava8Lambdas() {
        List<Object> noNullList = listWithNull.stream().filter(ele -> ele != null).collect(toList
            ());
        assertThat(noNullList).hasSize(1);

        List<Object> noNullList2 = listWithNull.parallelStream().filter(ele -> ele != null).collect
            (toList());
        assertThat(noNullList2).hasSize(1);

        //經過這些stream操作後，原來的list不會有變化
        assertThat(listWithNull).hasSize(3);
    }

}
