package test.networking;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * URLReader description
 *
 * @author johnson
 * @since 2015-03-13
 */
public class URLReader {
    public static void main(String[] args) throws Exception {

        URL oracle = new URL("http://www.oracle.com/");
        BufferedReader in = new BufferedReader(
            new InputStreamReader(oracle.openStream()));

        String inputLine;
        while ((inputLine = in.readLine()) != null)
            System.out.println(inputLine);
        in.close();
    }
}
