package guaveCache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.junit.Ignore;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * TestGuavaCache description
 *
 * @author johnson
 * @since 2015-05-06
 */
public class TestGuavaCache {

    private CacheLoader<String, String> strUppercaseLoader = new CacheLoader<String, String>() {
        @Override
        public String load(String key) throws Exception {
            return key.toUpperCase();
        }
    };

    @Test
    public void getStringUpperCaseFromCache() {
        LoadingCache<String, String> strUppercaseCache = CacheBuilder.newBuilder().build
            (strUppercaseLoader);

        assertEquals(0, strUppercaseCache.size());
        assertEquals("STRING", strUppercaseCache.getUnchecked("string"));
        assertEquals(1, strUppercaseCache.size());
    }

    @Test
    public void evictBySize() {
        LoadingCache<String, String> strCache = CacheBuilder.newBuilder()
            .maximumSize(3)
            .build(strUppercaseLoader);

        strCache.getUnchecked("first");
        strCache.getUnchecked("second");
        strCache.getUnchecked("third");
        strCache.getUnchecked("fourth");
        assertEquals(3, strCache.size());
        assertNull(strCache.getIfPresent("first"));
        assertEquals("FOURTH", strCache.getIfPresent("fourth"));
    }

    @Test
    @Ignore
    public void evictByWriteTime() throws InterruptedException {
        LoadingCache<String, String> strCache = CacheBuilder.newBuilder()
            .expireAfterWrite(5, TimeUnit.MILLISECONDS)
            .build(strUppercaseLoader);

        strCache.getUnchecked("first");
        assertEquals(1, strCache.size());

        strCache.getUnchecked("first");
        Thread.sleep(500);

//        strCache.getUnchecked("test");
        assertEquals(0, strCache.size());
        assertNull(strCache.getIfPresent("first"));
    }
}
