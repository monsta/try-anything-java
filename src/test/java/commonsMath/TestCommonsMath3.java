package commonsMath;

import org.apache.commons.math3.util.CombinatoricsUtils;
import org.junit.Test;

/**
 * TestCommonsMath3 description
 *
 * @author johnson
 * @since 2015-05-06
 */
public class TestCommonsMath3 {

    @Test
    public void combinatoricsUtils() {
        long coefficient = CombinatoricsUtils.binomialCoefficient(30, 4);
        System.out.println(coefficient);
    }
}
