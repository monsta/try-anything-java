package ip2countrycode;

import org.junit.Test;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by johnson on 2016/8/3.
 */
public class TestIPv4 {
    @Test
    public void toLong() {
        long aLong = IPv4.toLong("1.34.210.10");
        System.out.println(aLong);
        try {
            System.out.println(new BigInteger(1, InetAddress.getByName("255.255.255.255").getAddress()));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void ipv4ToBigInteger() throws UnknownHostException {
        InetAddress address = InetAddress.getByName("223.203.232.0");
        byte[] inet4Address = address.getAddress();
        System.out.println(new BigInteger(1, inet4Address));

        byte[] inet6Address = InetAddress.getByName("2c0f:ff98:ffff:ffff:ffff:ffff:ffff:ffff").getAddress();
        System.out.println(new BigInteger(inet6Address));

        InetAddress inet = InetAddress.getByAddress(
                new BigInteger("-540284928").toByteArray());
        System.out.println(inet.getHostAddress());
    }

    @Test
    public void ipv4Range() throws UnknownHostException {
        String target = "10.10.5.2";
        String ipStart = "10.0.0.0";
        String ipEnd = "10.12.0.0";

        BigInteger ipStartInt = ipToInt(ipStart);
        BigInteger ipEndInt = ipToInt(ipEnd);
        BigInteger targetInt = ipToInt(target);

        assertTrue(isBetween(targetInt, ipStartInt, ipEndInt));
    }

    @Test
    public void ipv6Range() throws UnknownHostException {
        String target = "2a06:536a:800::417a";
        String ipStart = "2a06:5348::";
        String ipEnd = "2a06:537f:ffff:ffff:ffff:ffff:ffff:ffff";

        BigInteger ipStartInt = ipToInt(ipStart);
        BigInteger ipEndInt = ipToInt(ipEnd);
        BigInteger targetInt = ipToInt(target);

        assertTrue(isBetween(targetInt, ipStartInt, ipEndInt));
    }

    @Test
    public void testIPV4_notInRange() {
        String target = "140.119.10.25";
        String ipStart = "10.0.0.0";
        String ipEnd = "10.12.0.0";

        BigInteger ipStartInt = ipToInt(ipStart);
        BigInteger ipEndInt = ipToInt(ipEnd);
        BigInteger targetInt = ipToInt(target);

        assertFalse(isBetween(targetInt, ipStartInt, ipEndInt));
    }

    @Test
    public void testIPV6_NotInRange() {
        String target = "2a06:5287::";
        String ipStart = "2a06:5348::";
        String ipEnd = "2a06:537f:ffff:ffff:ffff:ffff:ffff:ffff";

        BigInteger ipStartInt = ipToInt(ipStart);
        BigInteger ipEndInt = ipToInt(ipEnd);
        BigInteger targetInt = ipToInt(target);

        assertFalse(isBetween(targetInt, ipStartInt, ipEndInt));
    }

    private boolean isBetween(BigInteger targetInt, BigInteger lowerBoundInclusive, BigInteger upperBoundInclusive) {
        return targetInt.compareTo(lowerBoundInclusive) >= 0 && targetInt.compareTo(upperBoundInclusive) <= 0;
    }

    private BigInteger ipToInt(String ipAddress) {
        try {
            return new BigInteger(1, InetAddress.getByName(ipAddress).getAddress());
        } catch (UnknownHostException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
