package tryException;

import org.junit.Test;

/**
 * WriteListOfNumbersToFile description
 *
 * @author johnson
 * @since 2015-06-11
 */
public class WriteListOfNumbersToFile {

    @Test
    public void execute() throws Throwable {
        ListOfNumbers listNumbers = new ListOfNumbers();
        listNumbers.writeList();
        System.out.println(listNumbers.readFirstLine());
    }
}
