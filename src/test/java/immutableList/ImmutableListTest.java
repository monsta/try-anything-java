package immutableList;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections4.ListUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * ImmutableListTest description
 *
 * @author johnson
 * @since 2015-04-20
 */
public class ImmutableListTest {

    List<String> originalList = new ArrayList<>(Arrays.asList("one", "two", "three"));

    @Test(expected = UnsupportedOperationException.class)
    public void listWithJDK() {
        List<String> unmodifiableList = Collections.unmodifiableList(originalList);
        unmodifiableList.add("four");
    }

    @Test
    public void listWithJDK_originalCanStillBeModified() {
        List<String> unmodifiableList = Collections.unmodifiableList(originalList);
        originalList.set(2, "four");
        assertEquals("four", unmodifiableList.get(2));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void listWithGuava() {
        ImmutableList<String> immutableList = ImmutableList.copyOf(originalList);
        immutableList.add("four");
    }

    @Test
    public void listWithGuava_originalListModifiedNotUpdateGuavaCopy() {
        ImmutableList<String> immutableList = ImmutableList.copyOf(originalList);
        originalList.set(0, "somethingelse");
        assertThat(immutableList.get(0)).isNotEqualTo(originalList.get(0));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void listWithApacheCommons() {
        List<String> unmodifiableList = ListUtils.unmodifiableList(originalList);
        unmodifiableList.add("four");
    }

    @Test
    public void listCopyWithApacheCommons_thenModifyOriginalList_wouldAffectCopiedListView() {
        List<String> unmodifiableList = ListUtils.unmodifiableList(originalList);
        originalList.set(0, "four");
        assertThat(unmodifiableList.get(0)).isEqualTo(originalList.get(0));
    }
}
