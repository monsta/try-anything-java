package dateTime;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class TestTimeCreation {

    @Test
    public void timestampToDateTimeAndBack() {
        DateTime dt = new DateTime(1512037800000L, DateTimeZone.UTC);
        System.out.println("to UTC: " + dt.toDateTime(DateTimeZone.UTC).toString());

        LocalDateTime ldt = LocalDateTime.of(2017, 11, 30, 2, 30, 0);
        System.out.println("local date time string: " + ldt.toString());
        long l = ldt.toEpochSecond(ZoneOffset.UTC) * 1000;
        System.out.println("local date time to utc epoch second: " + l);
    }
}
