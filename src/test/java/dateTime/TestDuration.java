package dateTime;

import org.junit.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * Created by johnson on 2015/12/16.
 */
public class TestDuration {

    public static final int HOURS_PER_DAY = 24;

    @Test
    public void testDurationToDay() {
        final LocalDateTime from = LocalDateTime.of(2014, Month.DECEMBER, 1, 0, 0, 0);
        final LocalDateTime to = LocalDateTime.of(2014, Month.DECEMBER, 10, 0, 0, 0);

        //如果用Duration.between，回傳的還是Duration物件，沒有單位
        final Duration duration = Duration.between(from, to);
        assertThat(duration.toDays()).isEqualTo(9);
        assertThat(duration.toHours()).isEqualTo(HOURS_PER_DAY * 9);

        //如果程式邏輯直接就知道要用什麼單位衡量duration，可以改用ChronoUnit
        final long durationDays = ChronoUnit.DAYS.between(from, to);
        final long durationHours = ChronoUnit.HOURS.between(from, to);

        assertThat(durationDays).isEqualTo(9);
        assertThat(durationHours).isEqualTo(HOURS_PER_DAY * 9);
    }

    @Test
    public void test_MonthDay() {
        MonthDay feb9th = MonthDay.of(2, 9);

        assertThat(feb9th.getMonthValue()).isEqualTo(2);
        assertThat(feb9th.getDayOfMonth()).isEqualTo(9);
    }

    @Test
    public void betweenDays() {
        LocalDate ld1 = LocalDate.parse("2017-04-01", DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDate ld2 = LocalDate.parse("2017-04-10", DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDate ld3 = LocalDate.parse("2017-07-22", DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDate ld4 = LocalDate.parse("2017-04-02", DateTimeFormatter.ISO_LOCAL_DATE);

        System.out.println(ChronoUnit.DAYS.between(ld1, ld4));
        System.out.println(ChronoUnit.DAYS.between(ld1, ld2));
        System.out.println(YearMonth.from(ld1).equals(YearMonth.from(ld2)));
        System.out.println(YearMonth.from(ld1).equals(YearMonth.from(ld3)));
    }

    @Test
    public void yearMonthEquals() {
        YearMonth ym1 = YearMonth.parse("20170101", DateTimeFormatter.BASIC_ISO_DATE);
        YearMonth ym2 = YearMonth.parse("2017-01-01", DateTimeFormatter.ISO_LOCAL_DATE);
        assertEquals(ym1, ym2);
    }

}
