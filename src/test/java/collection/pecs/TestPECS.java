package collection.pecs;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestPECS {

    @Test
    public void testCovariant() {
        String[] s = new String[1];
        s[0] = "1";
        Object[] b = s;

        assertEquals("1", b[0]);
        assertThrows(ArrayStoreException.class, () -> {
            b[0] = 1;
        });
    }

    @Test
    public void testProducer() {
        List<Double> v1 = Arrays.asList(1.0, 2.0, 3.0);
        assertEquals(6.0, sum(v1));
        List<Integer> v2 = Arrays.asList(2, 3, 4);
        assertEquals(9, sum(v2));

        List<Integer> v3 = Arrays.asList(1, 2, 3);
        List<? extends Number> v4 = v3;
    }

    private double sum(List<? extends Number> list) {
        double rtn = 0.0;
        for (Number number : list) {
            rtn += number.doubleValue();
        }
        return rtn;
    }

    @Test
    public void testConsumer() {
        List<Integer> ints = new ArrayList<>();
        increment(ints, 6);
        assertEquals("[0, 1, 2, 3, 4, 5]", ints.toString());

        List<Number> nums = new ArrayList<>();
        increment(nums, 6);
        nums.add(6.0);
        assertEquals("[0, 1, 2, 3, 4, 5, 6.0]", nums.toString());

        List<Object> objs = new ArrayList<>();
        increment(objs, 6);
        objs.add("six");
        assertEquals("[0, 1, 2, 3, 4, 5, six]", objs.toString());
        int int2 = (Integer)objs.get(0);
        assertEquals(0, int2);
    }

    private void increment(List<? super Integer> consumer, int n) {
        for (int i = 0; i < n; i++) {
            consumer.add(i);
        }
    }
}
