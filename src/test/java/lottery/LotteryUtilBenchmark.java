package lottery;

import com.google.common.collect.Sets;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

/**
 * LotteryUtilBenchmark description
 *
 * @author johnson
 * @since 2015-06-18
 */
@State(Scope.Thread)
public class LotteryUtilBenchmark {

    private static LotteryUtil lotteryUtil = new LotteryUtil();

    @Benchmark
    public void test() {
        lotteryUtil.getCombinationOutOf(
            Sets.newHashSet("01", "02", "03", "04", "05", "06", "07", "08",
                "09", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"),
            5);
    }

    /**
     * 使用jmh對getCombinationOutOf()這個method做benchmarking
     *
     * @param args
     * @throws RunnerException
     */
    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
            .forks(0)                   //跟ant的fork一樣意思，設為0表示不另起VM去執行banchmarking
            .mode(Mode.AverageTime)     //benchmark的結果中要取哪一種數據，使處設定平均執行時間
            .warmupIterations(3)        //對於要測的method會先執行幾次做熱身
            .include("test")            //要執行的benchmark對象的regex pattern
            .measurementIterations(10)  //總共執行幾次以取得benchmark結果
            .build();
        new Runner(opt).run();
    }
}
