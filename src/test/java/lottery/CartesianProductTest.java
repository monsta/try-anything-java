package lottery;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * 笛卡兒積的測試
 *
 * @author johnson
 * @since 2015-05-20
 */
public class CartesianProductTest {

    @Test
    public void twoSetsTest() {
        Set<String> first = ImmutableSet.of("41", "42", "43", "44", "45");
        Set<String> second = ImmutableSet.of("31", "32", "33", "34", "35");
        Set<String> third = ImmutableSet.of("06", "16", "17");
        Set<String> fourth = ImmutableSet.of("12", "13", "01", "02");

        Set<List<String>> cp = Sets.cartesianProduct(first, second, third, fourth);
        System.out.println("==>cartesian product result count: " + cp.size());

        cp.stream().sequential()
            .peek(list -> System.out.println(list))
            .count();
    }
}
