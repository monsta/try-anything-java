package lottery;

import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * 測試連碰組合產生器
 *
 * @author johnson
 * @since 2015-05-06
 */
public class LotteryUtilTest {

    @Test
    public void getCombinationOutOfTest() throws InterruptedException {
        //選了一堆不同的數字
        Set<String> numbers = Sets.newHashSet("01", "02", "03", "04", "05", "06", "07", "08",
            "09", "10",
            "11","12","13","14","15","16","17","18","19","20",
            "21","22","23","24","25","26","27","28","29","30",
            "31","32","33","34","35","36","37","38","39","40");
//            "41","42","43","44","45","46");
        //丟進一個神奇的method
        List<Combination> combinations = new LotteryUtil().getCombinationOutOf(numbers, 5);
        System.out.println(combinations.size());
    }
}
