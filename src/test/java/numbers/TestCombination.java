package numbers;

import com.google.common.collect.Sets;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * class description of TestCombination
 *
 * @author johnson
 */
public class TestCombination {

    @Test
    public void test() {
        Set<Set<Integer>> allCombination = new HashSet<>();
        List<Integer> originalNumbers = Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);

        Set<Integer> numbers = Sets.newHashSet(originalNumbers);
        int n = numbers.size();
        int select = 3;
        long resultCount = Combinations.choose(n, select);
        for (int i = 0; i < resultCount; i++) {
            Set<Integer> comb = new TreeSet<>();
            for (int x : Combinations.element(n, select, i))
                comb.add(originalNumbers.get(x));
            allCombination.add(comb);
        }

        for (Set<Integer> comb : allCombination) {
            System.out.println(comb);
        }
        System.out.println("size: " + allCombination.size());
    }

    @Test
    public void calculateCoefficient() {
        System.out.println(CombinatoricsUtils.binomialCoefficient(15, 3));
    }

    @Test
    public void contantRampUp() {
        assertEquals(2, rampup(2, 1));
        assertEquals(12, rampup(2, 3));
        System.out.println(rampup(50, 60 * 50));
    }

    private int rampup(int per, int duration) {
        if (duration == 1) {
            return per;
        } else {
            int result = 0;
            for (int i = duration; i > 0 ; i--) {
                result += per * i;
            }
            return result;
        }
    }
}
