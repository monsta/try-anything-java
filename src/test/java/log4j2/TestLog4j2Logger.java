package log4j2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

/**
 * TestLog4j2Logger description
 *
 * @author johnson
 * @since 2015-05-06
 */
public class TestLog4j2Logger {

    private static final Logger LOG = LogManager.getLogger(TestLog4j2Logger.class);

    @Test
    public void logSomething() {
        LOG.info("this is a string with variable: {}", "hahaha~");
    }
}
