package fibonacci;

import java.math.BigInteger;

/**
 * Fibonacci最直覺簡單但也是時間複雜度最大(久)的實作
 *
 * @author johnson
 * @since 2015-07-21
 */
public class FibonacciSimple {

    public BigInteger fib(int n) {
        if(n < 2) {
            return BigInteger.valueOf(n);
        } else {
            return fib(n-1).add(fib(n-2));
        }
    }
}
