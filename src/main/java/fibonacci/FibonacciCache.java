package fibonacci;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * 一樣是使用遞迴的方式，但把從最小的n開始把運算結果都存在陣列cache中
 * 這樣只有在cache中不存在對應的fibonacci value時，才會做計算
 *
 * @author johnson
 * @since 2015-07-21
 */
public class FibonacciCache {
    private static List<BigInteger> CACHE = new ArrayList<>();
    static {
        CACHE.add(BigInteger.ZERO);
        CACHE.add(BigInteger.ONE);
    }

    public BigInteger fib(int n) {
        if(n >= CACHE.size()) {
            CACHE.add(n, fib(n-1).add(fib(n-2)));
        }
        return CACHE.get(n);
    }
}
