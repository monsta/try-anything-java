package fibonacci;

import java.math.BigInteger;

/**
 * 使用迴圈實作，時間複雜度接近線性
 *
 * @author johnson
 * @since 2015-07-21
 */
public class FibonacciIteration {

    public BigInteger fib(int n) {
        BigInteger prev1 = BigInteger.ZERO;
        BigInteger prev2 = BigInteger.ONE;

        for(int i = 0; i < n; i++) {
            BigInteger temp = prev1;
            prev1 = prev2;
            prev2 = temp.add(prev2);
        }

        return prev1;
    }
}
