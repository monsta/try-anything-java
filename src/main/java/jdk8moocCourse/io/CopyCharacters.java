package jdk8moocCourse.io;

import java.io.*;

/**
 * CopyCharacters description
 *
 * @author johnson
 * @since 2015-08-24
 */
public class CopyCharacters {

    public static void main(String[] args) throws IOException {
        BufferedReader in = null;
        PrintWriter out = null;

        try {
            in = new BufferedReader(new FileReader("/Users/johnson/Downloads/xanadu.txt"));
            out = new PrintWriter(new FileWriter("/Users/johnson/Downloads/outline.txt"));
            String c;

            while ((c = in.readLine()) != null) {
                out.println(c);//會直接使用執行期作業系統的預設斷行符號
            }
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }
}
