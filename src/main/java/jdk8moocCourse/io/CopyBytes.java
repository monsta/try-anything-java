package jdk8moocCourse.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * CopyBytes description
 *
 * @author johnson
 * @since 2015-08-24
 */
public class CopyBytes {

    public static void main(String[] args) throws IOException {
        FileInputStream fin = null;
        FileOutputStream fout = null;

        try {
            fin = new FileInputStream("/Users/johnson/Downloads/xanadu.txt");
            fout = new FileOutputStream("/Users/johnson/Downloads/outagain.txt");
            int c;

            while ((c = fin.read()) != -1) {
                fout.write(c);
            }
        } finally {
            if (fin != null) {
                fin.close();
            }
            if (fout != null) {
                fout.close();
            }
        }
    }
}
