package jdk8moocCourse.io;

import java.nio.file.FileSystems;
import java.nio.file.Path;

/**
 * RootDirTest description
 *
 * @author johnson
 * @since 2015-08-27
 */
public class RootDirTest {

    public static void main(String[] args) {
        Iterable<Path> roots = FileSystems.getDefault().getRootDirectories();
        for (Path root : roots) {
            System.out.println(root);
        }
    }
}
