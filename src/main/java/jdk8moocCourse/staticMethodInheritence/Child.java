package jdk8moocCourse.staticMethodInheritence;

/**
 * Child description
 *
 * @author johnson
 * @since 2015-07-23
 */
public class Child {

    static void importantMethod() {
        System.out.println("important in Child class!");
    }
}
