package jdk8moocCourse.staticMethodInheritence;

/**
 * Parent description
 *
 * @author johnson
 * @since 2015-07-23
 */
public class Parent {

    static void importantMethod() {
        System.out.println("important in Parent class!");
    }
}
