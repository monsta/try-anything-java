package jdk8moocCourse.staticMethodInheritence;

/**
 * Main description
 *
 * @author johnson
 * @since 2015-07-23
 */
public class Main {

    public static void main(String[] args) {
        Child.importantMethod();
        Parent.importantMethod();
        new Child().importantMethod();
        //no way the Child instance can call Parent static method
    }
}
