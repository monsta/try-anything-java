import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * MathGame description
 *
 * @author johnson
 * @since 2015-03-18
 */
public class MathGame {

    private Set<Integer> answer;
    private Set<Integer> answeredBucket;

    /**
     * 設定數個正整數謎底，大小範圍為1-100，且不重複
     *
     * @param answerCount 謎底有幾個數字
     */
    public void setPuzzle(int answerCount) {
        answer = new HashSet<>();
        answeredBucket = new HashSet<>(answer);
        while (answer.size() < answerCount) {
            answer.add((int) ((Math.random() * 100) + 1));
        }
        System.out.println("answers are: " + answer.toString());
    }

    /**
     * 猜數字，猜過繼續，全部猜完印出你已經猜完了...等訊息
     *
     * @param guess
     */
    public void guess(String guess) {

        if (answeredBucket.size() == answer.size()) {
            System.out.println("你已經猜完了，是還想怎樣");
        } else {
            try {
                Integer guessed = Integer.parseInt(guess);
                if(answer.contains(guessed)) {
                    answeredBucket.add(guessed);
                    System.out.println("你猜中了");
                } else {
                    System.out.println("你猜錯囉~~XD");
                }
            } catch (NumberFormatException e) {
                System.out.println("you gotta be kidding me...認真一點好嗎?");
            }
        }
    }
}
