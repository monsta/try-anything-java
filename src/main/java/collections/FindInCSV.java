package collections;

import java.io.*;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class FindInCSV {
    public static void main(String[] args) throws IOException {
        String badDeviceLogToCheck = "/Users/johnson/Downloads/bad.txt";
        BufferedReader reader = new BufferedReader(new FileReader(Paths.get(badDeviceLogToCheck).toFile()));

        String line = null;
        Scanner scanner = null;
        Set<String> badLoginSet = new HashSet<>();
        while ((line = reader.readLine()) != null) {
            scanner = new Scanner(line);
            scanner.useDelimiter(" ");
            while (scanner.hasNext()) {
                String data = scanner.next();
                if (data.matches("'dev[0-9]+'$")) {
                    badLoginSet.add(data.replace("'", ""));
                    break;
                }
            }
        }
        System.out.println(badLoginSet.size() + " bad devices");

        updateNewCSV(badLoginSet, "/Users/johnson/workspace/dqa-env/gatling/load-test-hamv2/src/test/resources/data/data.csv", "/Users/johnson/workspace/dqa-env/gatling/load-test-hamv2/src/test/resources/data/data_1.csv");
    }

    private static void updateNewCSV(Set<String> badLoginSet, String oldCSVPath, String newCSVPath) throws IOException {
        BufferedReader oldCSVReader = new BufferedReader(new FileReader(Paths.get(oldCSVPath).toFile()));
        BufferedWriter newCSVWriter = new BufferedWriter(new FileWriter(Paths.get(newCSVPath).toFile()));

        String csvline = null;
        Scanner csvscanner = null;
        while ((csvline = oldCSVReader.readLine()) != null) {
            csvscanner = new Scanner(csvline);
            csvscanner.useDelimiter(",");
            String deviceId = csvscanner.next();
            if (badLoginSet.contains(deviceId)) {
                System.out.println("bad log in: " + deviceId);
            } else {
                newCSVWriter.append(csvline).append("\n");
            }
        }
        oldCSVReader.close();
        newCSVWriter.close();
    }
}
