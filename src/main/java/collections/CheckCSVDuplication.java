package collections;

import java.io.*;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class CheckCSVDuplication {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(Paths.get("/Users/johnson/workspace/dqa-env/gatling/load-test-hamv2/src/test/resources/data/data.csv").toFile()));

//        Set<String> duplicatedLines = new HashSet<>();
        int duplicateCount = 0;
        String line = null;
        Scanner scanner = null;
        Set<String> nrSet = new HashSet<>();
        Set<String> nrRows = new HashSet<>();
        while ((line = reader.readLine()) != null) {
            scanner = new Scanner(line);
            scanner.useDelimiter(",");
            while (scanner.hasNext()) {
                String data = scanner.next();
                if (nrSet.contains(data)) { // check duplication
                    System.out.println("Duplicate nr:" + data);
//                    duplicatedLines.add(line);
                    duplicateCount++;
                    break;
                } else {
                    nrSet.add(data);
                    nrRows.add(line);
                    break;
                }
            }
        }

        System.out.println("###### duplicate count: " + duplicateCount);

        BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/johnson/workspace/dqa-env/gatling/load-test-hamv2/src/test/resources/data/data2.csv"));
        String newline = null;
        for (String noRepeatLine : nrRows) {
            writer.append(noRepeatLine);
            writer.newLine();
        }
    }
}
