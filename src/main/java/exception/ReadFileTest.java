package exception;

import java.io.*;

/**
 * ReadFileTest description
 *
 * @author johnson
 * @since 2015-06-30
 */
public class ReadFileTest {

    public static void main(String[] args) {
        String filePath = "/Users/johnson/Downloads/test.txt";
        cat(new File(filePath));
    }

    private static void cat(File file) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
            System.out.println("功能執行完畢");
        }
    }
}
