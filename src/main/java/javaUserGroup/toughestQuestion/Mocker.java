package javaUserGroup.toughestQuestion;

import java.sql.SQLException;

/**
 * Mocker description
 *
 * @author johnson
 * @since 2015-07-30
 */
public class Mocker<T extends Exception> {
    private void pleaseThrow(final Exception t) throws T {
        throw (T) t;
    }

    public static void main(String[] args) {
        try {
            new Mocker<RuntimeException>()
                .pleaseThrow(new SQLException());
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }
}
