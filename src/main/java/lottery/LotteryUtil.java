package lottery;

import com.google.common.collect.Lists;
import org.apache.commons.math3.util.CombinatoricsUtils;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * LotteryUtil description
 *
 * @author johnson
 * @since 2015-05-20
 */
public class LotteryUtil {

    /**
     * 產生所有連碰組合，每個組合儲存在{@link Combination}物件裡面
     *
     * @param numbers 所有選擇的號碼
     * @param stars   要產生幾星
     * @return
     */
    public <T> List<Combination> getCombinationOutOf(Set<T> numbers, int stars) {
        List<Combination> result = Lists.newArrayList();
        List<T> selectedNumbers = Lists.newArrayList(numbers);

        // 取得index的所有組合，ex:c3取2-->[0,1],[0,2],[1,2]
        Iterator<int[]> indexCombinationIterator = CombinatoricsUtils.combinationsIterator(
            numbers.size(), stars);

        indexCombinationIterator.forEachRemaining(next -> {
            Combination combination = new Combination(stars);
            for (int i = 0; i < next.length; i++) {
                combination.add(selectedNumbers.get(next[i]));
            }
            result.add(combination);
        });

        return result;
    }
}
