package lottery;

import java.util.HashSet;
import java.util.Set;

/**
 * 此class的實體代表單一個碰的數字組合
 *
 * @author johnson
 * @since 2015-05-06
 */
public class Combination<T> {

    private Set<T> numbers;

    public void add(T number) {
        this.numbers.add(number);
    }

    /**
     * numberCount表示這個組合會有幾個數字，就是幾碰，會在建構子中
     * 直接初始化對應長度的list做為內部儲存空間
     *
     * @param numberCount
     */
    public Combination(int numberCount) {
        this.numbers = new HashSet<>(numberCount);
    }

    @Override
    public String toString() {
        return "Combination{" +
            "numbers=" + numbers +
            '}';
    }
}
