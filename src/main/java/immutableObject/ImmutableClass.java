package immutableObject;

import java.util.Date;

/**
 * ImmutableClass description
 *
 * @author johnson
 * @since 2015-07-21
 */
public final class ImmutableClass {
    private final String strVal;
    private final Integer intVal;
    private final Date dateVal;

    private ImmutableClass(String strVal, Integer intVal, Date dateVal) {
        this.strVal = strVal;
        this.intVal = intVal;
        this.dateVal = dateVal;
    }

    public static final ImmutableClass newInstance(String strVal, Integer intVal, Date dateVal) {
        return new ImmutableClass(strVal, intVal, dateVal);
    }

    public String getStrVal() {
        return strVal;
    }

    public Integer getIntVal() {
        return intVal;
    }

    public Date getDateVal() {
        return new Date(dateVal.getTime());
    }

    @Override
    public String toString() {
        return "ImmutableClass{" +
            "strVal='" + strVal + '\'' +
            ", intVal=" + intVal +
            ", dateVal=" + dateVal +
            '}';
    }
}
