package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

/**
 * BasicSelenium description
 *
 * @author johnson
 * @since 2015-07-15
 */
public class BasicSelenium {

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = new PhantomJSDriver();
        driver.get("http://www.google.com");

        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys("ifalo");
        element.submit();
        System.out.println("Page title is: " + driver.getTitle());

        driver.quit();
    }
}
