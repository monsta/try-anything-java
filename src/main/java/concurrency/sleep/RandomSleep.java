package concurrency.sleep;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * class description of RandomSleep
 *
 * @author johnson
 */
public class RandomSleep {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService exec = Executors.newCachedThreadPool();

        List<Future<Integer>> sleepResults = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            sleepResults.add(exec.submit(new SleepTask()));
        }
        exec.shutdown();

        for (Future<Integer> srs : sleepResults) {
            try {
                System.out.println("sleep for " + srs.get() + " seconds");
            } catch (ExecutionException e) {
                e.printStackTrace();
                return;
            }
        }
        System.out.println("main thread end!");
    }

    static class SleepTask implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            int sleepFor = new Random().nextInt(10);
            TimeUnit.SECONDS.sleep(sleepFor);
            return sleepFor;
        }
    }
}
