package concurrency.sleep;

/**
 * class description of TestSleep
 *
 * @author johnson
 */
public class TestSleep {
    public static void main(String[] args) throws InterruptedException {
        Thread.sleep(2000);
        System.out.println("Main ended!");
    }
}
