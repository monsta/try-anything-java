package concurrency.sleep;

import java.util.concurrent.TimeUnit;

/**
 * class description of TestTimeUnitSleep
 *
 * @author johnson
 */
public class TestTimeUnitSleep {
    public static void main(String[] args) throws InterruptedException {
        TimeUnit.SECONDS.sleep(3);
        System.out.println("Main ended after 3 seconds!");
    }
}
