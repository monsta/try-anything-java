package concurrency.timsort;

import java.util.Arrays;

/**
 * TimSortShow description
 *
 * @author johnson
 * @since 2015-12-16
 */
public class TimSortShow {
    public static void main(String[] args) {
        //with a null in array, Arrays.sort would show "timsort" in the exception stacktrace
        String obj = null;
        String[] arr = new String[]{obj, "string"};
        Arrays.sort(arr);
    }
}
