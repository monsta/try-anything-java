package concurrency.atomic;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * class description of AtomicEvenIntegerNeverFail
 *
 * @author johnson
 */
public class AtomicIntegerTest implements Runnable {

    private AtomicInteger i = new AtomicInteger(0);

    public int getValue() {
        return i.get();
    }

    @Override
    public void run() {
        while (true)
            evenIncrement();
    }

    private void evenIncrement() {
        i.addAndGet(2);
    }

    public static void main(String[] args) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                System.err.println("Aborting!");
                System.exit(0);
            }
        }, 5000);

        ExecutorService exec = Executors.newCachedThreadPool();
        AtomicIntegerTest test = new AtomicIntegerTest();
        exec.execute(test);
        while (true) {
            int val = test.getValue();
            if(val % 2 != 0) {
                System.out.println(val);
                System.exit(1);
            }
        }
    }
}
