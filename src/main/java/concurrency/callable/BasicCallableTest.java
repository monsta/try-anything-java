package concurrency.callable;

import concurrency.TaskWithResult;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * class description of BasucCallableTest
 *
 * @author johnson
 */
public class BasicCallableTest {
    public static void main(String[] args) {
        ExecutorService exec = Executors.newCachedThreadPool();
        List<Future<String>> results = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            results.add(exec.submit(new TaskWithResult(i)));
        }
        exec.shutdown();
        System.out.println("executor service has shutdown!");

//        下面這一行執行的話會丟出RejectedExecutionException，因為ExecutorService shutdown之後不能再接受task
//        results.add(exec.submit(new TaskWithResult(999)));

        for(Future<String> fs : results) {
            try {
                System.out.println(fs.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

    }
}
