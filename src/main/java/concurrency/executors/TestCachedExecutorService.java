package concurrency.executors;

import concurrency.LiftOffTask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * class description of TestCachedExecutorService
 *
 * @author johnson
 */
public class TestCachedExecutorService {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            executorService.execute(new LiftOffTask());
        }
        executorService.shutdown();
//        System.out.println("Main thread ended!");//這一行訊息有可能先出現
    }
}
