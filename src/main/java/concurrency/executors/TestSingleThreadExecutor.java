package concurrency.executors;

import concurrency.LiftOffTask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * class description of TestSingleThreadExecutor
 *
 * @author johnson
 */
public class TestSingleThreadExecutor {
    public static void main(String[] args) {
        ExecutorService exec = Executors.newSingleThreadExecutor();
        for (int i = 0; i < 5; i++) {
            exec.execute(new LiftOffTask());
        }
        exec.shutdown();
        System.out.println("Main thread end!");
    }
}
