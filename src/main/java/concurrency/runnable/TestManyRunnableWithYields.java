package concurrency.runnable;

public class TestManyRunnableWithYields {

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            new Thread(new Atask()).start();
        }
        System.out.println("Waiting for tasks to start");
    }

    static class Atask implements Runnable {

        private int myhash;

        public Atask() {
            this.myhash = this.hashCode();
        }

        public void run() {
            System.out.println(myhash + ":message1");
            Thread.yield();
            System.out.println(myhash + ":message2");
            Thread.yield();
            System.out.println(myhash + ":message3");
            Thread.yield();
        }

        @Override
        protected void finalize() throws Throwable {
            System.out.println(myhash + ":finalize!");
            super.finalize();
        }
    }
}
