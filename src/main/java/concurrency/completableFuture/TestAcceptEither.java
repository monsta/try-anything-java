package concurrency.completableFuture;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * class description of TestAcceptEither
 *
 * @author johnson
 */
public class TestAcceptEither {
    public static void main(String[] args) throws InterruptedException,
            ExecutionException {
        CompletableFuture<String> fasterResult = fastFetch();
        CompletableFuture<String> maybeSlowerResult = slowerFetch();
        CompletableFuture<Void> finalResult = fasterResult
                .acceptEither(maybeSlowerResult, s -> System.out.println("the" +
                        " real result is... " + s));
        finalResult.get();//一定要有這行
    }

    private static CompletableFuture<String> slowerFetch() throws
            InterruptedException {
        return CompletableFuture.supplyAsync(() -> {
            int wait = new Random().nextInt(6) + 1;
            System.out.println("slow wait--" + wait);
            try {
                TimeUnit.SECONDS.sleep(wait);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "slower result wait for " + wait;
        });
    }

    private static CompletableFuture<String> fastFetch() throws
            InterruptedException {
        return CompletableFuture.supplyAsync(() -> {
            int wait = new Random().nextInt(6) + 1;
            System.out.println("fast wait--" + wait);
            try {
                TimeUnit.SECONDS.sleep(wait);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "faster result wait for " + wait;
        });
    }
}
