package concurrency.completableFuture;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * class description of BasicCompletableFuture
 *
 * @author johnson
 */
public class SimpleCompletableFuture {
    public static void main(String[] args) throws ExecutionException,
            InterruptedException {
        CompletableFuture<List<Integer>> futureInt = CompletableFuture
                .supplyAsync(() -> {
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
//                    Integer.parseInt("abcdefg");
                    return "42";
                })
                .thenApply(sup -> {
                    return Arrays.asList(Integer.valueOf(sup) + 42);
                })
//                .exceptionally(ex -> "Houston, we have a problem: " + ex.getMessage())
                ;

        System.out.println("one");
        System.out.println("two");
        System.out.println("three");
//        futureInt.complete(50);
        System.out.println(futureInt.get());
    }
}
