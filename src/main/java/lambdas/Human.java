package lambdas;

/**
 * Human description
 *
 * @author johnson
 * @since 2015-04-16
 */
public class Human {
    private String name;
    private int age;

    public Human() {
        super();
    }

    public Human(String name, int age) {
        super();
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static int compareByNameAndAge(Human lhu, Human rhu) {
        if(lhu.name.equals(rhu.name)) {
            return lhu.age - rhu.age;
        } else {
            return lhu.name.compareTo(rhu.name);
        }
    }
}
