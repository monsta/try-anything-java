package stringDeduplication;

import java.util.LinkedList;

/**
 * 分別用以下兩種設定來測試string deduplication的效用
 * -Xmx256m -XX:+UseG1GC
 * -Xmx256m -XX:+UseG1GC -XX:+UseStringDeduplication -XX:+PrintStringDeduplicationStatistics
 *
 * @author johnson
 * @since 2015-07-14
 */
public class LotsofStrings {
    private static final LinkedList<String> LOTS_OF_STRINGS = new LinkedList<>();

    public static void main(String[] args) throws Exception {
        int iteration = 0;
        while (true) {
            for (int i = 0; i < 100; i++) {
                for (int j = 0; j < 1000; j++) {
                    LOTS_OF_STRINGS.add(new String("String " + j));
                }
            }
            iteration++;
            System.out.println("Survived Iteration: " + iteration);
            Thread.sleep(100);
        }
    }
}
