package tryException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * ListOfNumbers description
 *
 * @author johnson
 * @since 2015-06-11
 */
public class ListOfNumbers {

    private static final int SIZE = 10;
    private static final String FILE_NAME = "OutFile.txt";
    private final List<Integer> list;

    public ListOfNumbers() {
        list = new ArrayList<Integer>();
        IntStream.range(0, SIZE)
            .forEach(it -> list.add(it));
    }

    public void writeList() throws Throwable {
        // The FileWriter constructor throws IOException, which must be caught.
        try (PrintWriter out = new PrintWriter(new FileWriter(FILE_NAME))){
            for (int i = 0; i < SIZE; i++) {
                // The get(int) method throws IndexOutOfBoundsException, which must be caught.
                out.println("Value at: " + i + " = " + list.get(i));
            }
        } catch (IOException ioe) {
            throw new Throwable(ioe);
        }
    }

    /**
     * read first from OutFile.txt
     *
     * @return
     */
    public String readFirstLine() {
        try (BufferedReader reader = new BufferedReader(new FileReader(FILE_NAME))) {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void cat(File file) {
        RandomAccessFile input = null;
        String line = null;

        try {
            input = new RandomAccessFile(file, "r");
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
            return;
        } catch (FileNotFoundException e) {
            System.err.format("file %s not found%n", file);
        } catch (IOException e) {
            System.err.format(e.toString());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }
    }
}
